public with sharing class TodoController {
    @AuraEnabled(cacheable=true)
    public static List<ToDo__c> getTodoList(String searchPriority, String searchStatus, String searchValue) {
        return [
            SELECT
                Id,
                Name,
                Description__c,
                Category__c,
                Priority__c,
                Status__c,
                RecordType.Name,
                Owner.Name,
                (SELECT Name, Description__c, Is_Done__c FROM Sub_ToDos__r ORDER BY CreatedDate DESC)
                FROM ToDo__c
                WHERE Name LIKE :('%' + searchValue + '%')
                AND Priority__c LIKE :searchPriority
                AND Status__c LIKE :searchStatus
                ORDER BY CreatedDate DESC
        ];
    }
}
