import { LightningElement, api } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import TODO_OBJECT from "@salesforce/schema/ToDo__c";
import NAME_FIELD from "@salesforce/schema/ToDo__c.Name";
import DESCRIPTION_FIELD from "@salesforce/schema/ToDo__c.Description__c";
import CATEGORY_FIELD from "@salesforce/schema/ToDo__c.Category__c";
import PRIORITY_FIELD from "@salesforce/schema/ToDo__c.Priority__c";
import STATUS_FIELD from "@salesforce/schema/ToDo__c.Status__c";
import userId from "@salesforce/user/Id";

export default class PopUpCreateTodo extends LightningElement {
    @api isPopUpOpen;
    defaultRadio = "012090000009hrcAAA";
    selectedRadioValue;
    objectApiName = TODO_OBJECT;
    fields = [
        NAME_FIELD,
        DESCRIPTION_FIELD,
        CATEGORY_FIELD,
        PRIORITY_FIELD,
        STATUS_FIELD
    ];
    recordTypePopUp = true;
    mainCreatePopUp = false;

    get recordTypeId() {
        if (this.selectedRadioValue != null) {
            return this.selectedRadioValue;
        }
        if (userId == "00509000005BdYPAA0") {
            return "012090000009hrhAAA";
        }
        if (userId == "00509000005Bdo1AAC") {
            return "012090000009hrcAAA";
        }
    }

    handleSuccess(event) {
        this.closeCreatePopUp();
        const toastEvent = new ShowToastEvent({
            title: "Todo created",
            message: "Record ID: " + event.detail.id,
            variant: "success"
        });
        this.dispatchEvent(toastEvent);
        const createEvent = new CustomEvent("create");
        this.dispatchEvent(createEvent);
    }

    closeCreatePopUp() {
        this.recordTypePopUp = true;
        this.mainCreatePopUp = false;
        this.selectedRadioValue = null;
        const closePopUpEvent = new CustomEvent("close");
        this.dispatchEvent(closePopUpEvent);
    }

    get isSystAdmin() {
        return userId == "00509000005BbcPAAS" && this.recordTypePopUp;
    }

    get isOpenCreateWindow() {
        return this.isSystAdmin == false || this.mainCreatePopUp;
    }

    get optionsRecord() {
        return [
            { label: "Personal", value: "012090000009hrcAAA" },
            { label: "Business", value: "012090000009hrhAAA" }
        ];
    }

    handleRadio(event) {
        this.selectedRadioValue = event.detail.value;
    }

    handleTypeChoose() {
        this.recordTypePopUp = false;
        this.mainCreatePopUp = true;
    }
}
