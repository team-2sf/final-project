import { LightningElement, api } from "lwc";

export default class TodoSubItems extends LightningElement {
    @api todoSubs;
    selectedItem;

    handleSelect(event) {
        this.selectedItem = event.detail.name;
    }

    get finalSubList() {
        return this.todoSubs.map(curr => {
            const { Is_Done__c, Name, Id } = curr;
            const Icon = Is_Done__c ? "utility:success" : "utility:close";
            return {
                Name,
                Id,
                Is_Done__c,
                Icon
            };
        });
    }
}
