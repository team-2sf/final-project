import { LightningElement } from 'lwc';

export default class TodoApp extends LightningElement {
    searchPriority = '%';
    searchStatus = '%';
    searchValue = '';

    handleSearch(event) {
        this.searchPriority = event.detail.priorityValue;
        this.searchStatus = event.detail.statusValue;
        this.searchValue = event.detail.search;
    }
}
