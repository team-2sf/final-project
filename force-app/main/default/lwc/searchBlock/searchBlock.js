import { LightningElement } from "lwc";

const delay = 300;

export default class SearchBlock extends LightningElement {
    priorityValue = "%";
    statusValue = "%";
    searchValue = "";

    handleSearchChange(event) {
        window.clearTimeout(this.delayTimeout);
        const searchValue = event.target.value;
        this.delayTimeout = setTimeout(() => {
            this.searchValue = searchValue;
            this.sendSearchEvent();
        }, delay);
    }

    handleChangePriorityRadio(event) {
        this.priorityValue = event.detail.value;
        this.sendSearchEvent();
    }

    handleChangeStatusRadio(event) {
        this.statusValue = event.detail.value;
        this.sendSearchEvent();
    }

    sendSearchEvent() {
        const searchRequest = {
            priorityValue: this.priorityValue,
            statusValue: this.statusValue,
            search: this.searchValue
        };
        const searchEvent = new CustomEvent("search", {
            detail: searchRequest
        });
        this.dispatchEvent(searchEvent);
    }

    get priorityOptions() {
        return [
            { label: "All", value: "%" },
            { label: "Low", value: "Low" },
            { label: "Medium", value: "Medium" },
            { label: "High", value: "High" }
        ];
    }

    get statusOptions() {
        return [
            { label: "All", value: "%" },
            { label: "Created", value: "Created" },
            { label: "In progress", value: "In progress" },
            { label: "Done", value: "Done" }
        ];
    }
}
